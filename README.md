# Nginx Cerberus

![](https://gitlab.com/zar3bski/nginx_cerberus/badges/master/pipeline.svg) 
![](https://img.shields.io/docker/image-size/zar3bski/nginx_cerberus)
![](https://img.shields.io/docker/v/zar3bski/nginx_cerberus)


An alpine based Nginx image powered by various security related optimisations

## Components

* [Nginx](https://www.nginx.com/)
* [ModSecurity v3](https://github.com/SpiderLabs/ModSecurity)
* [OWASP Core Rule Set](https://coreruleset.org/)
* [Certbot](https://certbot.eff.org/)

## Usage

```yaml
services:
  nginx: 
    image: zar3bski/nginx_cerberus
    volumes: 
      - ./site.conf:/etc/nginx/conf.d/site.conf
    ports: 
      - 80:80
      - 443:443
    environment: 
      - ADMIN_EMAIL=some@email.com
```

### Env VARs

|name           |required  |default      |description|
|:-------------:|:--------:|:------------|:----------|
|`ADMIN_EMAIL`  | **True** | None        | expiration email so Let's Encrypt could notify the administrator (required only if `AUTO_HTTPS` == True)|
|`AUTO_HTTPS`   |**False** | True        | generate ssl certificates for **all** `domaines` found in `/etc/nginx/conf.d` and migrate to HTTPS
|`LOG_STDOUT`   |**False** | True        | whether or not default logs should be appended to `stdout` rather than files |

### server configuration files

Here is how to activate Modsecurity for a website. 
```
server {
    # ...
    modsecurity on;
    modsecurity_rules_file /etc/nginx/modsec/mod_sec_main.conf;
}

```

`mod_sec_main.conf` includes all [OWASP core rules](https://github.com/coreruleset/coreruleset/tree/v3.3/dev/rules) stored in `/etc/nginx/modsec/owasp_rules`. You could either point to `/etc/nginx/modsec/crs-setup.conf` or to some `custom.conf` (provided that it includes `modsecurity.conf`). For instance:

```toml
Include "/etc/nginx/modsec/modsecurity.conf"

SecRule ARGS:testparam "@contains test" "id:1234,deny,status:403"
```